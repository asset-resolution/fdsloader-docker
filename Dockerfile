FROM ubuntu:16.04
LABEL "Name"="Asset Resolution FactSet Loader"
LABEL "Version"="0.1.0"
LABEL "Maintainer"="Alex@Asset-Resolution.com"

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
  # netcat is needed for the wait-for script
    libexpat1 \
    odbc-postgresql \
    postgresql-client \
    unixodbc \
    unzip \
      iodbc \
      libiodbc2 \
      libiodbc2-dev \
      libpq-dev \
      libssl-dev \
      odbcinst1debian2 \
      odbc-postgresql \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY ./docker/hostdata/FDSLoader-Linux-2.13.2.0.zip .
RUN unzip FDSLoader-Linux-2.13.2.0.zip -d /tmp/FDSLoader

COPY ./docker/hostdata/odbc.ini /root/.odbc.ini
COPY ./docker/hostdata/odbcinst.ini /etc/odbcinst.ini

CMD ["/tmp/FDSLoader/FDSLoader64"]
