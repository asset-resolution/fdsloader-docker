# Docker container for FactSet Data Loader

To get started (in a development environment):

1. Generate `key.txt` and place in `docker/hostdata`, and `chmod 777 docker/hostdata/key.txt`, to ensure the file is writable in the docker volume
2. Place `FDSLoader64-Linux-2.13.2.0.zip` in `docker/hostdata`, so iit  can be installed when building the docker image.
3. `docker-compose up --build` will build the docker container, and by default start a Postgres database in docker (at `db:5432`). The connnection information for the default testing database is already loaded in `odbc.ini` and `odbcinst.ini` in the loader container.
4. (local development) `docker attach fdsloader-docker_fds_loader_1` (in a new terminal) will srop your session inside the docker container.
5. Inside the docker container, `/tmp/FDSLoader/FDSLoader64 --setup` will run the setup commands to generate `config.xml`. This file is persisted in a volume, so setup only needs to be run once. `config.xml` is accessible on the host machine at `docker/hostdata`, so configs can be shared between containers.
6. On the host machine, `cat docker/hostdata/key.txt`, and make note of the counter.
7. Inside the docker container, `/tmp/FDSLoader/FDSLoader64 --test` runs the data loader configuration tests.
7. On the host machine, `cat docker/hostdata/key.txt`, and ensure the counter has incremented properly.
8. Inside the docker container, `/tmp/FDSLoader/FDSLoader64` runs the data loader. **NOTE: This currently fails.**

If you need to transfer files between host and container (such as support logs), `docker/hostdata` on the host machine is attached as a volume, and syncs to `/tmp/hostdata` inside the container.
